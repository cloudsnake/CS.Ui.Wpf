﻿using System;

namespace CS.Ui.Wpf.Controls;

public interface ISingleOpen : IDisposable
{
    bool CanDispose { get; }
}
