﻿namespace CS.Ui.Wpf.Data;

public enum BadgeStatus
{
    Text,
    Dot,
    Processing
}
