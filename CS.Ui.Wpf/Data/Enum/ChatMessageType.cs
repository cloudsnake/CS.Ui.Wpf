﻿namespace CS.Ui.Wpf.Data;

public enum ChatMessageType
{
    String,
    Image,
    Audio,
    Custom
}
