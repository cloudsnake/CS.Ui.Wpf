﻿namespace CS.Ui.Wpf.Data;

public enum ChatRoleType
{
    Sender,
    Receiver
}
