﻿namespace CS.Ui.Wpf.Data;

public enum ColLayoutStatus
{
    Xs,
    Sm,
    Md,
    Lg,
    Xl,
    Xxl,
    Auto
}
