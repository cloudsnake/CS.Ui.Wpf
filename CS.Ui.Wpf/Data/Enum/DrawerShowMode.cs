﻿namespace CS.Ui.Wpf.Data;

public enum DrawerShowMode
{
    Cover,
    Push,
    Press
}
