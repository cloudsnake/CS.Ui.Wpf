﻿namespace CS.Ui.Wpf.Data;

public enum GrowlShowMode
{
    Prepend,
    Append
}
