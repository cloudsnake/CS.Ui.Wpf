﻿namespace CS.Ui.Wpf.Data.Enum;

public enum HitMode
{
    Click,
    Hover,
    Focus,
    None
}
