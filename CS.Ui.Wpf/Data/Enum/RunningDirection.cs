﻿namespace CS.Ui.Wpf.Data;

public enum RunningDirection
{
    EndToStart,
    StartToEnd
}
