﻿namespace CS.Ui.Wpf.Data;

public enum ShowAnimation
{
    None,
    HorizontalMove,
    VerticalMove,
    Fade
}
