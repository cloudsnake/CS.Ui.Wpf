﻿namespace CS.Ui.Wpf.Data;

public enum SideMenuItemRole
{
    Header,
    Item
}
