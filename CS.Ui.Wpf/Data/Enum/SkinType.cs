﻿
namespace CS.Ui.Wpf.Data;

public enum SkinType
{
    Default,
    Dark,
    Violet
}
