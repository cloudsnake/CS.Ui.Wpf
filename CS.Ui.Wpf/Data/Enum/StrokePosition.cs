﻿namespace CS.Ui.Wpf.Data;

public enum StrokePosition
{
    Center,
    Outside,
    Inside
}
