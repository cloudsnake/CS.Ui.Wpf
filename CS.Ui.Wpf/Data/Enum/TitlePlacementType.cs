﻿
namespace CS.Ui.Wpf.Data;

/// <summary>
///     标题对齐方式
/// </summary>
public enum TitlePlacementType
{
    Left,
    Top
}
