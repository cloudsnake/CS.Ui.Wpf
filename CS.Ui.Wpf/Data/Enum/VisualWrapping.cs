﻿namespace CS.Ui.Wpf.Data;

public enum VisualWrapping
{
    NoWrap,
    Wrap
}
